package com.trossky.businesses;

import com.trossky.entities.Test;

import java.util.ArrayList;
import java.util.List;

public class TestBusiness {

    public List<Test> getTestList() {
        List<Test> testList= new ArrayList<>();
        for (int i = 0; i <10; i++) {
            testList.add(new Test(i,1,"Article "+(i+1), "Description number "+(i+1)));
        }
        return testList;
    }
}
