package com.trossky.resources;


import com.codahale.metrics.annotation.Timed;
import com.trossky.businesses.TestBusiness;
import com.trossky.entities.Test;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tests")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TkyResource {
    private TestBusiness testBusiness;

    public TkyResource(TestBusiness testBusiness) {
        this.testBusiness = testBusiness;
    }

    @GET
    @Timed
    public Response getTestList(){
        Response response;
        List<Test> testList = testBusiness.getTestList();
        response = Response.status(Response.Status.OK).entity(testList).build();
        return response;
    }

}
