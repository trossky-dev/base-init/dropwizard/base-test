package com.trossky;

import com.trossky.businesses.TestBusiness;
import com.trossky.resources.TkyResource;
import io.dropwizard.Application;
import io.dropwizard.forms.MultiPartBundle;
import io.dropwizard.java8.Java8Bundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

public class TkyAPI extends Application<TkyConfiguration> {

    public static void main(String[] args) throws Exception {
        new TkyAPI().run(args);

    }


    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void initialize(Bootstrap<TkyConfiguration> bootstrap) {
        bootstrap.addBundle(new Java8Bundle());
        bootstrap.addBundle(new MultiPartBundle());

        super.initialize(bootstrap);
    }

    @Override
    public void run(TkyConfiguration tkyConfiguration, Environment environment) throws Exception {
        environment.jersey().setUrlPattern("/api/v1/*");

        // Enable CORS headers
        final FilterRegistration.Dynamic corsFilter =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_ORIGINS_PARAM, "*");
        corsFilter.setInitParameter(CrossOriginFilter.ACCESS_CONTROL_ALLOW_ORIGIN_HEADER, "*");
        corsFilter.setInitParameter("allowedHeaders", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin");
        corsFilter.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "OPTIONS,GET,PUT,POST,DELETE_PRODUCTS_OFFER,HEAD");
        corsFilter.setInitParameter("allowCredentials", "true");

        // Add URL mapping
        corsFilter.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        final TestBusiness testBusiness= new TestBusiness();

        environment.jersey().register(new TkyResource(testBusiness));
    }
}
