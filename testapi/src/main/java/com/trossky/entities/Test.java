package com.trossky.entities;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

public class Test {

    private Integer id;
    private Integer type;
    private String title;
    private String body;

    @JsonCreator
    public Test(@JsonProperty("id") Integer id,@JsonProperty("type") Integer type,@JsonProperty("title") String title,@JsonProperty("body") String body) {
        this.id = id;
        this.type = type;
        this.title = title;
        this.body = body;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
